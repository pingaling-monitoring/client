package pingaling

import (
	"fmt"
	"strings"
)

// Endpoint
type Endpoint struct {
	URL            string         `json:"url"`
	NextCheck      string         `json:"next_check"`
	Name           string         `json:"name"`
	Status         string         `json:"status"`
	Description    string         `json:"description"`
	HealthStatuses []HealthStatus `json:"health_statuses"`
}

// EndpointsData list of endpoints with health status
type EndpointsData struct {
	Data []Endpoint `json:"data"`
}

// EndpointData single endpoint with health status
type EndpointData struct {
	Data Endpoint `json:"data"`
}

func (endpointData EndpointsData) FormatList() FormattedData {
	headers := []string{
		"Name",
		"Status",
		"Next check",
		"Url",
	}

	data := make([]string, 0)

	for _, endpoint := range endpointData.Data {
		row := []string{
			endpoint.Name,
			endpoint.Status,
			FormatDate(endpoint.NextCheck),
			endpoint.URL,
		}

		data = append(data, strings.Join(row, "\t"))
	}

	return FormattedData{
		Headers: headers,
		Rows:    data,
	}
}

func (endpointData EndpointData) FormatShow() FormattedData {
	headers := []string{
		"Name",
		"Status",
		"Next check",
		"Url",
		"Description",
	}

	endpoint := endpointData.Data

	data := fmt.Sprintf(
		"%s\t%s\t%s\t%s\t%s",
		endpoint.Name,
		endpoint.Status,
		FormatDate(endpoint.NextCheck),
		endpoint.URL,
		endpoint.Description,
	)

	return FormattedData{
		Headers: headers,
		Rows:    []string{data},
	}
}

func (endpointData EndpointData) PrintDescribe() {
	data := endpointData.Data
	healthStatuses := make([]string, 0)
	healthStatusHeaders := []string{
		"Status",
		"Updated At",
	}

	for _, healthStatus := range data.HealthStatuses {
		row := []string{
			healthStatus.Status,
			FormatDate(healthStatus.UpdatedAt),
		}

		healthStatuses = append(healthStatuses, strings.Join(row, "\t"))
	}
	seperator := "\n\n"

	formattedHealthStatuses := FormattedData{
		Headers: healthStatusHeaders,
		Rows:    healthStatuses,
	}
	endpointDetails := fmt.Sprintf(
		"ENDPOINT\n%s%s"+
			"URL\n%s%s"+
			"DESCRIPTION\n%s\n",
			data.Name, seperator,
		data.URL, seperator,
		data.Description,
	)

	nextCheck := fmt.Sprintf("Next check: %s", FormatDate(data.NextCheck))

	fmt.Println(endpointDetails)
	PrintTable(formattedHealthStatuses)
	fmt.Println()
	fmt.Println(nextCheck)
}
