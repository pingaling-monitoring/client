package pingaling

type FormattedData struct {
	Headers []string
	Rows    []string
}
