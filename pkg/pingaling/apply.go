package pingaling

import (
	"fmt"
	"io/ioutil"
	"log"
)

func ApplyManifest(filename string, session *Session) {
	if content, err := ioutil.ReadFile(filename); err != nil {
		log.Fatalf("failed to read manifest: %v, %v", filename, err)
	} else {
		// Split the YAML base on '---'
		docs, err := SplitYAMLDocuments(content)
		if err != nil {
			log.Fatalf("Failed to split manifests: %v, %v", content, err)
		} else {
			// Post manifest to API
			for _, d := range docs {
				buf, manErr := session.ApplyManifest(d)
				if manErr != nil {
					log.Printf("Failed to create resource %+v. Error: %v", d, manErr)
				} else {
					fmt.Println(buf.String())
				}
			}
		}
	}
}
