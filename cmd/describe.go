// Copyright © 2018 The Pingaling Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	"log"
)

var describeCmd = &cobra.Command{
	Use: "describe",
	Short: "Describe a specific resource in detail",
	Long: "Shows all information about a specific resource",
	Example: `
	# Describe an endpoint
	pingaling describe endpoint foobar
`,
}

var describeEndpointCmd = &cobra.Command{
	Use: "endpoint",
	Short: "Describe a specific endpoint",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) < 1 {
			return errors.New("Missing endpoint name")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		endpointName := args[0]

		endpoint, err := session.GetEndpoint(endpointName)
		if err != nil {
			log.Fatal("failed to get endpoint '", endpointName, "': ", err)
		}

		endpoint.PrintDescribe()
	},
}

func init() {
	rootCmd.AddCommand(describeCmd)
	describeCmd.AddCommand(describeEndpointCmd)
}
